用户管理
=================

参考文档：\ `细说Linux系统用户和组管理 <http://www.cnblogs.com/f-ck-need-u/p/7011460.html#auto_id_9>`_\ 

此处介绍用户和组的相关概念





.. toctree::
   :titlesonly:
   :glob:


   1-user/index
   2-group/index
   3-susudo/index
