磁盘管理
=================


df查看磁盘的使用情况

.. code-block:: sh

	$df -h


mount挂载设备

.. code-block:: sh

	$mount /dev/sdb1 /mnt/data       # 将磁盘sdb的主分区1挂载到/mnt/data目录下

注意：

- umount卸载设备时需要退出挂载目录，否则卸载时会提示设备busy无法卸载，因为该设备正在被占用
- mount将设备挂载到已存在数据的目录时，会临时覆盖目录中原有的数据；当设备卸载后，原有数据就会恢复



.. toctree::
   :titlesonly:
   :glob:


   1-basicopl/index
   2-lvm/index
   3-raid/index
