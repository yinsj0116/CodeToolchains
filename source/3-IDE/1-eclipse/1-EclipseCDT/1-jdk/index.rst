JDK环境安装
=================

\ ``Eclipse``\ 是基于\ ``Java``\ 的可扩展开源开发平台，所以安装\ ``Eclipse``\  前需要确保电脑已安装\ ``JDK``\ 

\ ``JDK``\ 的安装可参考：\ `java开发环境配置 <http://www.runoob.com/java/java-environment-setup.html>`_\ 

Windows下安装java开发环境的一般步骤是：

- \ `下载JDK <#downloadl>`_\ 
- \ `安装JDK <#installl>`_\ 
- \ `配置环境变量 <#configurel>`_\ 
- \ `验证安装成功 <#conforml>`_\ 

.. _downloadl:

0x00 下载JDK
~~~~~~~~~~~~~~

官方下载地址：\ `JDK下载 <http://www.oracle.com/technetwork/java/javase/downloads/index.html>`_\ 

.. figure:: ../images/1.png

在下载页面需要选择接受许可，并根据自己的系统选择对应的版本

.. figure:: ../images/2.png

.. _installl:

0x01 安装JDK
~~~~~~~~~~~~~~

windows上的程序安装最简单，直接一直下一步默认安装就行

安装\ ``JDK``\ 的时候默认会安装\ ``JRE``\ ，其实\ ``JDK``\ 里面已经自带\ ``JRE``\ 环境，可以不用安装它

安装\ ``JDK``\ ，安装过程中可以自定义安装目录等信息

.. figure:: ../images/3.png

.. figure:: ../images/4.png


.. _configurel:

0x02 配置环境变量
~~~~~~~~~~~~~~~~~~~

\ ``JDK``\ 安装后需要配置系统环境变量，便于随处随时都可以使用\ ``JDK``\ 环境

右击\ ``我的电脑``\ ，点击\ ``属性``\ ，选择\ ``高级系统设置``\ 

.. figure:: ../images/5.png

选择\ ``高级``\ 选项卡，点击\ ``环境变量``\ 

.. figure:: ../images/6.png

然后就会出现如下图所示的画面

.. figure:: ../images/7.png

在\ ``系统变量``\ 中设置2项属性，\ ``JAVA_HOME``\ 、\ ``PATH``\ (大小写无所谓)即可；若已存在则点击\ ``编辑``\ ，不存在则点击\ ``新建``\ 

变量设置参数如下

.. csv-table::
	:header: 变量名, 变量值
	:widths: 30, 50

	\ ``JAVA_HOME(新建)``\ , \ ``D:\Program Files\Java\jdk-10.0.1``\ (要根据自己的实际路径配置)
	\ ``Path(编辑)``\ , \ ``%JAVA_HOME%\bin;D:\Program Files\Java\jre-10.0.1\bin;``\ 

注意：

- 在\ ``Windows10``\ 中，因为系统的限制，\ ``path``\ 变量只可以使用\ ``JDK``\ 的绝对路径；\ ``%JAVA_HOME%``\ 会无法识别，导致配置失败
- \ ``JAVA_HOME``\ 变量只是设置被\ ``Path``\ 变量引用，也可以删除，直接使用绝对路径即可
- \ ``Path``\ 变量后面的\ ``D:\Program Files\Java\jre-10.0.1\bin``\ 路径可有可无，因为\ ``%JAVA_HOME%\bin``\ 已经包含了\ ``JRE``\ 环境
- 如果使用1.5以上版本的\ ``JDK``\ ，不用设置\ ``CLASSPATH``\ 环境变量，也可以正常编译和运行Java程序。

新建\ ``JAVA_HOME``\ 变量

.. figure:: ../images/8.png

编辑\ ``Path``\ 变量

.. figure:: ../images/9.png


.. _conforml:

0x03 验证安装成功
~~~~~~~~~~~~~~~~~~~~

键入命令\ ``java --version``\ 、\ ``java``\ 、\ ``javac``\ 几个命令，能够回显相关信息，说明环境变量配置成功

.. figure:: ../images/10.png

输入\ ``where java``\ 命令可以查看\ ``java``\ 的查找路径有哪些

.. figure:: ../images/11.png