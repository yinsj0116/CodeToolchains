Eclipse CDT安装
=================

安装完\ ``JDK``\ 环境后，我们再来安装\ ``Eclipse``\ 

官方下载地址：\ `Eclipse <https://www.eclipse.org/downloads/>`_\ 

.. figure:: ../images/12.png

选择国内的下载镜像，这样下载速度比较快

.. figure:: ../images/13.png

.. figure:: ../images/14.png

下载将会自动开始，若没反应，则可以点击\ ``click here``\ 手动下载

.. figure:: ../images/15.png

这样下载下来的是一个\ ``Eclipse Installer``\ 安装器，而不是离线安装包，双击它，弹出安装页面，可以选择各种不同的语言的开发环境(包括\ ``Java``\ 、\ ``C/C++``\ 、\ ``JavaEE``\ 、\ ``PHP``\ 等)，这里我选择\ ``C/C++``\ 

.. figure:: ../images/16.png

选择安装目录

.. figure:: ../images/17.png

选定安装目录后，点击\ ``INSTALL``\ 即可，接下来我们等待安装完成就可以使用了

.. figure:: ../images/18.png

