软件包管理器rpm
=================

接下来将从以下几个方面来介绍如何通过软件包管理器rpm来进行软件包相关管理操作

- \ `rpm包管理器 <#rpml>`_\ 
- \ `获取rpm包 <#rgetpac>`_\ 
- \ `安装前校验 <#rprechech>`_\ 
- \ `rpm包安装 <#rinstall>`_\ 
- \ `安装后校验 <#rpostcheck>`_\ 
- \ `rpm包卸载 <#rpkgunin>`_\ 
- \ `rpm包查询 <#rpkgquery>`_\ 
- \ `rpm包升级 <#rpkgupdate>`_\ 
- \ `重建rpm数据库 <#rbuilddata>`_\ 


在没有软件包管理器前，用户都是通过源代码的方式来安装软件。但是我们很容易发现，在每次安装软件时都必须对操作系统的编译参数进行对应修改，并且操作过程很是复杂，这对于不熟悉操作系统的朋友来说真心困难，那么有没有一款软件能让用户能很简单的安装所需的软件呢？

通过社区与企业的共同努力，Linux开发商将固定的硬件平台和操作系统需要安装或升级的软件编译好，并且将这些软件通过特定的打包方式将软件打包成一个特殊格式的文件，这些软件的文件含有能检测操作系统环境和软件依赖性的脚本，并提供记载该软件提供的所有文件信息等，最后将这个软件发布出去供用户使用。那么用户得到这个软件包，通过特定的命令，就能执行检测系统环境，根据环境所需的要求，对软件进行安装。这就是软件管理器

包管理器的功能就是：将编译好的程序打包成一个文件或有限的几个文件，用于实现安装、卸载、升级、查询等功能

说白了，包管理器的本质就是：维护一个数据库，该数据库包含以下内容

- 安装前内容

	- 软件包的名称和版本
	- 软件包的依赖关系(包括循环依赖和版本依赖)
	- 提供功能性说明
- 安装后内容

	- 软件包安装时生成的各文件的路径
	- 软件包安装时生成的各文件的MD5校验码
- 程序组成清单

	- 文件清单：二进制文件、库文件、配置文件、帮助文档
	- 执行脚本：执行安装、升级、卸载等软件包管理操作时所需要执行的脚本文件

虽说包管理器已经很方便用户对软件包的管理，但是包管理器依然有以下几个缺陷：

- 无法自动解决程序包安装时的依赖关系
- 程序包不是官方最新的

目前常见的Linux软件包管理器有

- \ ``dpkg(Debian Package)``\ 

	- 开发者：\ ``Debian Linux社区``\ 
	- 包管理命令：\ ``dpkg``\ 
	- dpkg包的后缀：\ ``.deb``\ 
	- 适用\ ``dpkg``\ 的linux发行版：\ ``Debain``\ 、\ ``Ubuntu``\ 
- \ ``rpm(RadHat Package Manager)``\ 

	- 开发者：\ ``Red Hat公司``\ 
	- 包管理命令：\ ``rpm``\ 
	- rpm包的后缀：\ ``.rpm``\ 
	- 适用\ ``rpm``\ 的linux发行版：\ ``RedHat``\ 、\ ``SUSE``\ 

\ ``CentOS``\ 发行版派生于\ ``RedHat``\ ，它使用的包管理器就是\ ``rpm包管理器``\ 

.. _rpml:

0x00 rpm包管理器
~~~~~~~~~~~~~~~~~~

\ ``RPM``\ 包管理器的全称叫做\ ``RedHat Package Manager``\ ，由\ ``Red Hat``\ 公司开发出来的，由于非常的简单实用，很多的\ ``distributions``\ 都使用这个机制来安装和管理软件(例如：\ ``RedHat``\ 、\ ``SuSe``\ )

\ ``RPM``\ 程序包的组成格式为：\ ``name-version-release.arch.rpm``\ 

	- \ ``name``\ ：软件包的名字
	- \ ``version``\ ：软件的版本信息，组成部分为\ ``major.minor.release``\ 

		- \ ``major``\ ：主版本号
		- \ ``minor``\ ：次版本号
		- \ ``release``\ ：大版本的发行号，属于源码包的发行号
	- \ ``release``\ ：\ ``rpm包``\ 的发行号，与程序源码(\ ``version``\ 里的)的发行号无关，仅用于标识对\ ``rpm包``\ 不同制作的修订，比如升级了什么补丁；还包含其适用的\ ``OS(el6:redhat enterprise linux6、CentOS7、suse11)``\ 
	- \ ``arch``\ ：软件包适用的硬件平台

		- \ ``x86``\ ：\ ``i386``\ 、\ ``i486``\ 、\ ``i586``\ 、\ ``i686``\ 等
		- \ ``x86_64``\ ：\ ``x86_64``\ 
		- \ ``powerpc``\ ：\ ``ppc``\ 
		- \ ``noarch``\ ：没有任何硬件等级上的限制
	- \ ``rpm``\ ：rpm程序包的固定后缀名

假设一个程序有20个功能

- 常用功能有8个
- 特殊功能A：3个
- 特殊功能B：6个
- 二次开发相关功能：3个

如果用户只需要常用功能，可是必须要全部安装，那么就会很占用空间，而且其他功能根本不会使用，为了方便管理，于是就有了分包机制：\ ``核心包(主包)+子包(分包)``\ 

- 功能：将一个rpm大包按照不同的功能需求打包制作成多个rpm包
- 核心包：命令和源程序一致，例如\ ``bash-4.2.3-3.centos7.x86_64.rpm``\ 
- 子包：安装子包前必须安装核心包，例如\ ``bash-devel-4.2.3-3.centos7.x86_64.rpm``\ 

.. _rgetpac:

0x01 获取rpm包
~~~~~~~~~~~~~~~~

获取rpm包的方式有

- 挂载发行商光盘镜像获取

.. figure:: ../images/1.png

- 使用\ ``wget``\ 或\ ``ftp``\ 命令从指定站点下载获取

	- 国内镜像站点

		- \ `阿里aliyun <https://mirrors.aliyun.com/centos/>`_\ 
		- \ `网易163 <http://mirrors.163.com/centos/>`_\ 
		- \ `搜狐sohu <http://mirrors.sohu.com/centos/>`_\ 
		- \ `清华大学 <https://mirrors.tuna.tsinghua.edu.cn/centos/>`_\ 
		- \ `中国科技大学 <http://mirrors.ustc.edu.cn/centos/>`_\ 
		- \ `东软信息学院 <http://mirrors.neusoft.edu.cn/centos/>`_\ 
	- 第三方站点

		- \ `Fedora EPEL <https://dl.fedoraproject.org/pub/epel/>`_\ 
		- \ `rpmfind <ftp://fr2.rpmfind.net/>`_\ 
		- \ `rpmpbone <http://rpm.pbone.net>`_\ 
	- 项目官网

.. code-block:: sh

	$wget https://dl.fedoraproject.org/pub/epel/7/x86_64/b/bash-completion-extras-2.1-11.el7.noarch.rpm
	$lftp https://mirrors.aliyun.com/centos/


.. _rprechech:

0x02 安装前校验
~~~~~~~~~~~~~~~~~

安装前检验包括

- 程序包完整性校验：通过单向加密机制(\ ``MD5|sha1``\ )
- 程序包来源合法性校验：通过公钥加密机制(\ ``RSA``\ )

我们将使用\ ``rpm {-K|--checksig} /path/to/rpmpackages``\ 命令来校验程序包的完整性和来源合法性

- \ ``--nosignature``\ :不检查来源合法性
- \ ``--nodigest``\ ：不检查包完整性

.. figure:: ../images/2.png

以上校验失败是由于没有导入公钥造成的

我们可以使用\ ``rpm --import /path/to/gpg-key-file``\ 命令导入\ ``rpm包``\ 制作者提供的公钥，利用该公钥与制作rpm时使用的私钥进行校验程序包的来源合法性

- 如果待校验的rpm包来源是iso镜像，则\ ``gpg-key-file``\ 在iso镜像上与\ ``Packages/``\ 目录的同级目录中
- 如果待校验的rpm包来源是镜像站点，则\ ``gpg-key-file``\ 在镜像站点上与\ ``Packages/``\ 目录的同级目录中

.. figure:: ../images/6.png

.. figure:: ../images/4.png

接下来我们来导入公钥

.. figure:: ../images/5.png

最后就可以来校验rpm包了

.. figure:: ../images/3.png


.. _rinstall:

0x03 rpm包安装
~~~~~~~~~~~~~~~~~

rpm包的安装命令为：\ ``rpm -ivh Package_File``\ 

.. figure:: ../images/7.png

上述选项的含义是

- \ ``-i``\ ：install安装软件包操作
- \ ``-v``\ ：安装软件包时显示详细信息
- \ ``-vv``\ ：安装软件包时显示更详细信息
- \ ``-h``\ ：hash码，在安装过程中使用#号来显示安装进度

安装rpm包时还可以使用的选项有

- \ ``--replacepkgs``\ ：重新安装软件包，此时原来的配置文件不会被覆盖，新安装的配置文件将会重命名为以\ ``.rpmnew``\ 为后缀的文件
- \ ``--nodeps``\ ：安装软件包时忽略依赖关系，这样可能会导致安装后程序无法运行
- \ ``--test``\ ：测试安装软件包


.. _rpostcheck:

0x04 安装后校验 
~~~~~~~~~~~~~~~~

安装后校验就是：校验软件包安装后生成的文件是否被修改过

- \ ``rpm -V package_Name``\ 

如果上述命令无任何提示信息说明没有被修改；如果有提示信息，则提示信息含义如下(可以在rpm命令的man文档中查看)

- \ ``S file Size differs``\ ：大小改变了
- \ ``M Mode differs (includes permissions and file type``\ ：权限，文件类型改变
- \ ``5 digest (formerly MD5 sum) differs``\ ：MD5改变了
- \ ``D Device major/minor number mismatch``\ ：如果是设备文件，则主设备号和次设备号发生改变
- \ ``L readLink(2) path mismatch``\ ：路径发送改变
- \ ``U User ownership differs``\ ：属主发生改变
- \ ``G Group ownership differs``\ ：属组发生改变
- \ ``T mTime differs``\ ：时间发生改变
- \ ``P caPabilities differ``\ ：功能发生改变

.. figure:: ../images/8.png

.. _rpkgunin:

0x05 rpm包卸载
~~~~~~~~~~~~~~~~

rpm包卸载命令为：\ ``rpm -e Package_Name``\ 

\ ``-e``\ 选项表示卸载删除软件包；如果卸载时该包被其它软件包所依赖时，默认会将依赖于此包的所有包一并卸载；如果想忽略依赖关系，只卸载当前包，可以使用\ ``--nodeps``\ 选项，但是依赖于此包的程序可能运行不太正常

\ **注意**\ ：如果包的配置文件安装后曾被改动过，卸载时，此文件将不会卸载，而是重命名并保存，会出现下面的字段\ ``warning: /etc/zshrc saved as /etc/zshrc.rpmsave``\ 

.. _rpkgquery:

0x06 rpm包查询
~~~~~~~~~~~~~~~~~~~~

rpm包查询可以了解到很多信息

- 查询所有已安装的包(\ ``-qa``\ 选项后面可以使用\ ``globbing``\ 机制实现查询过滤)：\ ``rpm -qa [globbing]``\ 

.. figure:: ../images/9.png

- 查询包的描述信息：\ ``rpm -qi Package_Name``\ 

.. figure:: ../images/10.png

- 查询某单个包是否安装：\ ``rpm -q Package_Name``\ (没有安装会显示没有安装；安装的会显示安装版本)

.. figure:: ../images/11.png

- 查询安装包生曾的列表的位置：\ ``rpm -ql Package_Name``\ 

.. figure:: ../images/12.png

- 查询某文件是哪个包安装生成的：\ ``rpm -qf /path/to/file``\ 

.. figure:: ../images/13.png

- 查询包安装后生成的帮助文档：\ ``rpm -qd Package_Name``\ 

.. figure:: ../images/14.png

- 查询安装后生成的配置文件：\ ``rpm -qc Package_Name``\ 

.. figure:: ../images/15.png

- 查询包相关的脚本：\ ``rpm -q --scripts Package_Name``\ 脚本分为以下4类

	- \ ``preinstall``\ ：安装前脚本
	- \ ``postinstall``\ ：安装后脚本
	- \ ``preuninstall``\ ：卸载前脚本
	- \ ``postuninstall``\ ：卸载后脚本

.. figure:: ../images/16.png

- 查询尚未安装的rpm包文件的相关信息

	- 查询安装后会生成的文件列表：\ ``rpm -qpl /path/to/rpmpackage``\ 
	- 查询其简单描述信息：\ ``rpm -qpi /path/to/rpmpackage``\ 

.. figure:: ../images/17.png


.. _rpkgupdate:

0x07 rpm包升级
~~~~~~~~~~~~~~~

rpm包升级有两种方式

- \ ``rpm -Uvh /path/to/rpmpackage``\ ：升级或安装(如果有老版本就升级，如果没有就安装)，相当于安装新版本后会卸载所有的老版本
	
	- 升级的时候也可能会出现版本冲突等问题，所以如果想强制安装升级可以使用\ ``--force``\ 选项
- \ ``rpm -Fvh /path/to/rpmpackage``\ ：强制升级(如果有老版本也安装新版本)，相当于安装新版本后依然保留之前的老版本

	- 升级的时候也可能会出现版本冲突等问题，所以如果想强制安装升级可以使用\ ``--force``\ 选项

\ **注意**\ ：不要对内核执行升级操作；linux系统允许多版本内核可以并存，因此，建议执行安装操作

.. _rbuilddata:

0x08 重建rpm数据库
~~~~~~~~~~~~~~~~~~~~

rpm数据库文件存放的位置是：\ ``/var/lib/rpm``\ 

.. figure:: ../images/18.png

重建的方法有

- \ ``rpm --initdb``\ ：初始化数据库，如果事先不存在一个数据库，则新建之
- \ ``rpm --rebuilddb``\ ：重建数据库，直接新建数据库，会覆盖原有库