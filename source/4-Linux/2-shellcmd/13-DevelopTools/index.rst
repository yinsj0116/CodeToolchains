开发工具
=================

\ ``od``\ 查看二进制文件信息

.. code-block::sh

	$od -tc a.out    # 以ASCII码形式显示
	$od -td a.out    # 以有符号十进制数形式显示
	$od -tf a.out    # 以浮点数形式显示
	$od -to a.out    # 以八进制数形式显示
	$od -tu a.out    # 以无符号十进制数形式显示
	$od -tx a.out    # 以十六进制数显示