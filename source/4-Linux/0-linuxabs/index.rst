Linux概述
===========

\ ``Linux``\ 是\ ``类Unix``\ 计算机操作系统的统称

- 参考文档一：\ `Linux发展史及简介 <https://blog.csdn.net/zengxiantao1994/article/details/53141747>`_\ 
- 参考文档二：\ `Linux发展历史 <https://www.cnblogs.com/zcl1840/p/6473026.html>`_\ 

其发展历史可以简述为

- 1965年，Bell实验室、MIT、GE(通用电气公司)准备开发\ ``Multics系统``\ ，为了同时支持300个终端访问主机，但是1969年失败了；(刚开始并没有鼠标、键盘、输入设备只有卡片机，因此如果要测试某个程序，则需要将读卡纸插入卡片机，如果有错误，还需要重新来过 Multics：Multiplexed Information and Computing Service)
- 1969年，\ ``Ken Thompson(C语言之父)``\ 利用汇编语言开发了\ ``FIle Server System(Unics，即UNIX的原型)``\ ，因为汇编语言对于硬件的依赖性，因此只能针对特定硬件，然而只是为了移植一款太空旅游的游戏
- 1973年，\ ``Dennis Ritchie``\ 和\ ``Ken Thompson``\ 发明了C语言，而后写出了UNIX的内核(将B语言改成C语言，由此产生了C语言之父；90%的代码是C语言写的，10%的代码用汇编写的，因此移植时只要修改那10%的代码即可)
- 1977年，Berkeley大学的\ ``Bill Joy``\ 针对他的机器修改UNIX源码，称为\ ``BSD(Berkeley Software Distribution)``\ ，\ ``Bill Joy``\  是Sun公司的创始人
- 1979年，UNIX发布System V，用于个人计算机
- 1984年，因为UNIX规定：不能对学生提供源码；\ ``Tanenbaum``\ 老师自己编写兼容于UNIX的Minix，用于教学
- 1984年，\ ``Stallman``\ 开始\ ``GNU(GNU's Not Unix)``\ 项目，创办\ ``FSF(Free Software Foundation)``\ 基金会

	- 其产品有：GCC、Emacs、Bash Shell、GLIBC
	- GNU的软件缺乏一个开放的平台运行，只能在UNIX上运行
	- GNU倡导自由软件，即用户可以对软件做任何修改，甚至再发行，但是始终要挂着GPL的版权；自由软件是可以卖的，但是不能只卖软件，而是卖服务、手册等
- 1985年，为了避免GNU开发的自由软件被其他人用作专利软件，因此创建\ ``GPL(General Public License)``\ 版权声明
- 1988年，MIT为了开发GUI，成立了XFree86的组织
- 1991年，芬兰赫尔辛基大学的研究生\ ``Linus Torvalds``\ 基于gcc、bash开发了针对386机器的Linux内核
- 1994年，\ ``Torvalds``\ 发布Linux-v1.0
- 1996年，\ ``Torvalds``\ 发布Linux-v2.0，确定了Linux的吉祥物：企鹅

.. figure:: images/1.jpg

linux发展到现在，已经有诸多发行版本，如下图所示

- 参考文档：\ `linux发行版 <http://futurist.se/gldt/>`_\ 

.. figure:: images/1.png

在上述所示发行版中，广泛使用的有

- \ ``RedHat``\ 系列发行版：商业公司维护发行的版本，其包管理方式采用的是基于\ ``rpm``\ 的\ ``yum包管理``\ 方式

	- \ ``RHEL(Redhat Enterprise Linux)``\ ：也就是Redhat Advance Server收费版本，该发行版稳定性好，适用于服务器
	- \ ``CentOS``\ ：\ ``RHEL``\ 的社区克隆免费版本，该发行版稳定性好，适用于服务器
	- \ ``Fedora``\ ：由原来的Redhat桌面版本发展而来的免费版本，稳定性较差，最好只用于桌面应用
- \ ``Debian``\ 系列发行版：社区组织维护的发行版本，其包管理方式采用的是基于\ ``dpkg``\ 的\ ``apt-get包管理``\ 方式

	- \ ``Ubuntu``\ ：有应用于服务器的免费版本，也有应用于桌面的免费版本，具体如下

		- 命令规则

			- 前两位数字代表发行时年份的最后两位数字，称为主版本号；主版本号为单数年时是短期支持版本，主版本号为双数年时是长期支持版(\ ``LTS``\ )
			- 后两位代表发行的月份，称为副版本号；副版本号要么是4，要么是10；4代表4月份发布的稳定版，10代表10月份发布的测试版版，通常在稳定版中发现的漏洞，或是一些改进方案会放到10月份的测试版本中进行测试
		- 版本支持周期

			- 桌面(Desktop)LTS(Long Term Support)版本至少三年的技术支持
			- 服务器(Server)LTS(Long Term Support)版本至少五年的技术支持
		- 版本发布频率：一年2次；4月份稳定版一次，10月份测试版版一次

关于上述广泛使用的linux发行版，即可以在官网进行下载，又可以在相关镜像站点进行下载

- \ `阿里aliyun <https://opsx.alibaba.com/mirror>`_\ 
- \ `网易163 <http://mirrors.163.com/>`_\ 
- \ `搜狐sohu <http://mirrors.sohu.com/>`_\ 
- \ `清华大学 <https://mirrors.tuna.tsinghua.edu.cn/>`_\ 
- \ `中国科技大学 <http://mirrors.ustc.edu.cn/>`_\ 
- \ `东软信息学院 <http://mirrors.neusoft.edu.cn/>`_\ 

尽管linux的发行版如此众多，但是每种发行版的系统构造大致相同，如下图所示

.. figure:: images/2.jpg

其中

- 每种发行版的内核都是使用\ `linux内核 <https://www.kernel.org/>`_\ 
- 每种发行版提供的应用程序各不相同，包括

	- 编译器：\ ``gcc/g++``\ 等
	- 文本编辑器：\ ``vim/emacs``\ 等
	- GUI环境：\ ``KED(qt编写)``\ 、\ ``GNOME(GTK编写)``\ 、\ ``Unity``\ 等
	- CLI环境：\ ``shell``\ 等
- 库函数是对内核提供的内核态系统调用的用户态封装，给应用程序提供对应的内核入口；应用程序调用库函数，通过对应的系统调用进入内核态，完成对相应硬件设备的操作

对我们用户而言，与linux系统进行交互的方式无非就是两种：\ ``GUI环境``\ 和\ ``CLI环境``\ 

其中广泛使用的，也是身为码农必须掌握的交互方式非\ ``CLI环境的shell``\ 莫属

所以本系列文章将从以下三大章节围绕\ ``CLI环境之shell``\ 来展开介绍\ ``Linux工具集``\ 

- \ `shell环境 <../1-shellenv/index.html>`_\ ：主要介绍shell应用程序本身
- \ `shell命令 <../2-shellcmd/index.html>`_\ ：主要介绍以shell应用程序为命令解释器的各大linux命令应用程序
- \ `shell编程 <../3-shellprogram/index.html>`_\ ：主要介绍shell脚本的相关语法和相关示例脚本


\ **注意**\ 

- 本系列文章不包括身为c程序猿必须掌握的标准库和系统调用，这两部分的详解可参考：\ `c系统编程之常用类库 <http://systemprogramming.readthedocs.io/zh_CN/latest/2-library/index.html>`_\ 
- 本系列文章也不包括linux内核相关知识点，详解可参考


