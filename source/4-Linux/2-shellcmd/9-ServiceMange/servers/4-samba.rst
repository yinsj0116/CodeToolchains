samba服务器搭建
================

参考文档

- \ `安装与配置Samba服务器 <http://blog.51cto.com/11099293/2065389>`_\ 
- \ `samba文件共享服务配置 <http://www.cnblogs.com/lxyqwer/p/7271369.html>`_\ 
- \ `samba服务配置 <https://www.cnblogs.com/zoulongbin/p/7216246.html>`_\ 


Samba是在Linux和UNIX系统上实现SMB协议的一个免费软件，可以实现局域网内不同操作系统之间文件共享和打印机共享，由服务器及客户端程序构成

\ ``SMB(Server Messages Block信息服务块)``\ 是一种在局域网上共享文件和打印机的一种通信协议，它为局域网内的不同计算机之间提供文件及打印机等资源的共享服务，它侦听在445端口和139端口

SMB协议是客户端/服务器型协议，客户端通过该协议可以访问服务器上的共享文件系统、打印机及其他资源；通过设置\ ``NetBIOS over TCP/IP``\ 使得Samba不但能与局域网络主机分享资源，还能与全世界的电脑分享资源

samba服务器的搭建流程如下所示：

- \ `环境准备 <#envlll>`_\ 
- \ `samba软件安装 <#sainl>`_\ 
- \ `配置文件修改 <#conflll>`_\ 
- \ `添加samba用户 <#sambau>`_\ 
- \ `配置防火墙 <#firel>`_\ 
- \ `启动samba服务 <#servl>`_\ 
- \ `客户端访问 <#clils>`_\ 
- \ `常见FAQ <#faqll>`_\ 



.. _envlll:

0x00 环境准备
~~~~~~~~~~~~~~~~~~~~~~~~~

先确保系统已安装相关软件源

- RedHat/Centos系列发行版确认已安装\ `yum源 <../../5-PackageManage/1-centos/2-yum/index.html#repo>`_\ 
- Debian/Ubuntu系统发行版确认已安装\ ``apt-get源``\ 


.. _sainl:

0x01 samba软件安装
~~~~~~~~~~~~~~~~~~~~~~~~~

RedHat/Centos系列发行版通过以下步骤来安装samba软件

- \ ``yum install samba -y``\ ：安装samba软件
- \ ``rpm -qa samba``\ 或\ ``yum list installed samba``\ ：查询samba软件是否已经安装
- \ ``rpm -ql samba``\ ：查询安装samba软件时安装了哪些文件
- \ ``rpm -qc samba``\ ：查询安装samba软件时生成的配置文件

samba软件安装后，有以下几个文件需要特别关注，在之后的配置服务可能会用到

- \ ``/etc/samba/smb.conf``\ ：samba服务的主要配置文件
- \ ``/etc/samba/lmhosts``\ ：samba服务的域名设定，主要设置IP地址对应的域名，类似linux系统的\ ``/etc/hosts``\ 
- \ ``/etc/samba/smbusers``\ ：samba服务设置samba虚拟用户的配置文件
- \ ``/var/lib/samba/private/{passdb.tdb,secrets.tdb,smbpasswd}``\ ：存放samba的用户账号和密码数据库文档


.. _conflll:

0x02 配置文件修改
~~~~~~~~~~~~~~~~~~~~~~~~~

Samba的主配置文件叫\ ``smb.conf``\ ，默认在\ ``/etc/samba/``\ 目录下

\ ``smb.conf``\ 含有多个段

- 每个段由段名开始，直到下个段名，每个段名放在方括号中间
- 每段的参数的格式是：\ ``arg_name = arg_value``\ 
- 段名和参数一行一个，段名和参数名不分大小写

除了\ ``[global]``\ 段外，所有的段都可以看作是一个共享资源，段名是该共享资源的名字，段里的参数是该共享资源的属性

该配置文件的参数主要分为两大类

- 全局设置参数：\ ``[global]``\ 字段下的参数

	- \ ``config file = /usr/local/samba/lib/smb.conf.%m``\ ：该参数可以使用另一个配置文件来覆盖缺省(默认)的配置文件\ ``smb.conf``\ ；如果指定的文件不存在，则该项无效

		- 这个参数很有用，可以使得samba配置更灵活，可以让一台samba服务器模拟多台不同配置的服务器
		- 例如：PC1(主机名)这台电脑在访问\ ``Samba Server``\ 时使用它自己的配置文件，那么先在\ ``/etc/samba/host/``\ 下为\ ``PC1``\ 配置一个名为\ ``smb.conf.pc1``\ 的文件，然后在\ ``smb.conf``\ 中加入：\ ``config file = /etc/samba/host/smb.conf.%m``\ ；这样当PC1请求连接\ ``Samba Server``\ 时，\ ``smb.conf.%m``\ 就被替换成\ ``smb.conf.pc1``\ ；这样，对于PC1来说，它所使用的Samba服务就是由\ ``smb.conf.pc1``\ 定义的，而其他机器访问\ ``Samba Server``\ 则还是应用\ ``smb.conf``\ 
	- \ ``workgroup = WORKGROUP``\ ：设置\ ``Samba Server``\ 所要加入的工作组或者域；window的工作组一般为\ ``WORKGROUP``\ 
	- \ ``server string = Samba Server Version %v``\ ：主机的简易说明；默认没有设置该参数
	- \ ``netbios name = MYSERVER``\ ：设置主机的\ ``netBIOS``\ 名称，如果不填写则默认服务器DNS的一部分，\ ``workgroup``\ 和\ ``netbios name``\ 名字不要设置成一样；默认没有设置该参数
	- \ ``interfaces = lo eth0 192.168.12.2/24 192.168.13.2/24``\ ：设置samba服务器监听网卡，可以写网卡名称或IP地址；默认没有设置该参数
	- \ ``hosts allow = 127. 192.168.12. 192.168.13``\ ：设置允许连接到samba服务器的客户端；默认没有设置该参数
	- \ ``hosts deny =192.168.12.0/255.255.255.0``\ ：设置不允许连接到samba服务器的客户端；默认没有设置该参数
	- \ ``log level =1``\ ：日志文件安全级别，0~10级别，默认0；默认没有设置该参数
	- \ ``log file = /var/log/samba/%m``\ ：产生日志文件的命名，默认以访问者IP地址命名
	- \ ``max log size = 50``\ ：日志文件最大容量50，默认50，单位为KB，0表示不限制
	- \ ``security = user``\ ：设置用户访问samba服务器的验证方式 ，一共四种验证方式

		- \ ``share``\ ：用户访问\ ``Samba Server``\ 不需要提供用户名和口令, 安全性能较低(samba 4.2之后的版本不再支持该参数值)
		- \ ``user``\ ：\ ``Samba Server``\ 共享目录只能被授权的用户访问，由\ ``Samba Server``\ 负责检查账号和密码的正确性，账号和密码要在本\ ``Samba Server``\ 中建立，存放在\ ``/var/lib/samba/private/{passdb.tdb,secrets.tdb}``\ 文件中
		- \ ``server``\ ：依靠其他\ ``Windows NT/2000``\ 或\ ``Samba Server``\ 来验证用户的账号和密码，是一种代理验证；此种安全模式下，系统管理员可以把所有的Windows用户和口令集中到一个NT系统上，使用\ ``Windows NT``\ 进行\ ``Samba``\ 认证； 远程服务器可以自动认证全部用户和口令，如果认证失败，Samba将使用用户级安全模式作为替代的方式
		- \ ``domain``\ ：域安全级别，使用主域控制器(\ ``PDC``\ )来完成认证
	- \ ``passdb backend = tdbsam``\ ：定义用户后台类型

		- \ ``smbpasswd``\ ：使用SMB服务的\ ``smbpasswd``\ 命令给系统用户设置SMB密码建立samba用户，基于该用户进行账户验证；不过使用该命令建立samba用户必须先建立系统用户，否则samba找不到系统用户则创建失败；该命令主要维护\ ``/var/lib/samba/private/smbpasswd``\ 密码文件

			- \ ``adduser zhang``\ ：创建系统用户
			- \ ``passwd zhang``\ ：为系统用户设定密码；该密码可不用设置
			- \ ``smbpasswd –a zhang``\ ：将刚创建的系统用户设置为samba用户并设置密码
			- \ ``smbpasswd -d zhang``\ ：冻结刚创建的samba用户，该用户不能在登录了
			- \ ``smbpasswd -e zhang``\ ：恢复冻结的samba用户，让冻结的用户可以在使用
			- \ ``smbpasswd -n zhang``\ ：将samba用户的密码设置成空
			- \ ``smbpasswd -x zhang``\ ：删除samba用户
		- \ ``tdbsam``\ ：使用SMB服务的\ ``pdbedit``\ 命令直接建立SMB独立用户，基于该用户进行账户验证；该命令建立samba用户不需要先建立系统用户；该命令主要维护\ ``/var/lib/samba/private/{passdb.tdb,secrets.tdb}``\ 数据库文件

			- \ ``pdbedit –a username``\ ：新建Samba账户
			- \ ``pdbedit –x username``\ ：删除Samba账户
			- \ ``pdbedit –L``\ ：列出Samba用户列表，读取\ ``passdb.tdb``\ 数据库文件
			- \ ``pdbedit –Lv``\ ：列出Samba用户列表的详细信息
			- \ ``pdbedit –c "[D]" –u username``\ ：暂停该Samba用户的账号
			- \ ``pdbedit –c "[]" –u username``\ ：恢复该Samba用户的账号
		- \ ``ldapsam``\ ：基于\ ``LDAP``\ 服务进行账户验证
	- \ ``username map = /etc/samba/smbusers``\ ：配合\ ``/etc/samba/smbusers``\ 文件设置虚拟用户；默认没有设置该参数
	- \ ``encrypt passwords = yes|no``\ ：是否将认证密码加密；因为现在windows操作系统都是使用加密密码，所以一般要开启此项。不过配置文件默认已开启
	- \ ``smb passwd file = /etc/samba/smbpasswd``\ ：用来定义samba用户的密码文件；smbpasswd文件如果没有那就要手工新建
	- \ ``username map = /etc/samba/smbusers``\ ：用来定义用户名映射，比如可以将\ ``root``\ 换成\ ``administrator/admin``\ 等；不过要事先在\ ``smbusers``\ 文件中定义好；比如：\ ``root = administrator admin``\ ，这样就可以用\ ``administrator``\ 或\ ``admin``\ 这两个用户来代替\ ``root``\ 登陆\ ``Samba Server``\ ，更贴近windows用户的习惯
	- \ ``guest account = nobody``\ ：用来设置guest用户名
	- \ ``socket options = TCP_NODELAY SO_RCVBUF=8192 SO_SNDBUF=8192``\ ：用来设置服务器和客户端之间会话的Socket选项，可以优化传输速度
	- \ ``load printers = yes|no``\ ：设置是否在启动Samba时就共享打印机
	- \ ``printcap name = cups``\ ：设置共享打印机的配置文件
	- \ ``printing = cups``\ ：设置Samba共享打印机的类型；现在支持的打印系统有：\ ``bsd/sysv/plp/lprng/aix/hpux/qnx``\ 
- 共享资源设置参数：\ ``[ShareName]``\ 字段下的参数(\ ``ShareName``\ 为共享字段名，可自定义)

	- \ ``comment = This is share software``\ ：设置共享描述信息
	- \ ``path = /home/testfile``\ ：设置共享目录路径

		- 可以用\ ``%u``\ 、\ ``%m``\ 这样的宏来代替路径里的\ ``unix用户`` 和\ ``客户机的Netbios主机名``\ 
		- 用宏表示主要用于\ ``[homes]``\ 共享域
		- 例如：如果我们不打算用home段做为客户的共享，而是在\ ``/home/share/``\ 下为每个Linux用户以他的用户名建个目录，作为他的共享目录，这样path就可以写成\ ``path = /home/share/%u``\ ; 用户在连接到这共享时具体的路径会被他的用户名代替，要注意这个用户名路径一定要存在，否则，客户机在访问时会找不到网络路径。同样，如果我们不是以用户来划分目录，而是以客户机来划分目录，为网络上每台可以访问samba的机器都各自建个以它的netbios名的路径，作为不同机器的共享资源，就可以这样写\ ``path = /home/share/%m``\  
	- \ ``browseable = yes|no``\ ：设置共享目录是否可浏览，如果\ ``no``\ 就表示隐藏，需要通过\ ``IP+共享名称``\ 进行访问
	- \ ``writable = yes|no``\ ：设置共享目录是否具有可写权限
	- \ ``available = yes|no``\ ：设置该共享资源是否可用
	- \ ``read only = yes|no``\ ：设置共享目录是否具有只读权限
	- \ ``public = yes|no``\ ：设置共享目录是否允许guest账户访问
	- \ ``guest ok = yes|no``\ ：功能同public一样
	- \ ``create mask = 0775``\ ：创建的文件权限为700
	- \ ``directory mode = 0775``\ ：创建的目录权限为700
	- \ ``admin users = root``\ ：设置共享目录的管理员，如果\ ``security =share``\ 时，该项无效；多用户中间使用逗号隔开，例如\ ``admin users = root,user1,user2``\ 
	- \ ``valid users = username``\ ：设置允许访问共享目录的用户；例如\ ``valid users = user1,user2,@group1,@group2``\ (多用户或组使用逗号隔开，@group表示group用户组)
	- \ ``invalid users = username``\ ：设置不允许访问共享目录的用户
	- \ ``write list = username``\ ：设置在共享目录具有写入权限的用户，例如\ ``write list = user1,user2,@group1,@group2``\ (多用户或组使用逗号隔开，@group表示group用户组)


一般可以将\ ``/etc/samba/smb.conf``\ 主配置文件修改如下

.. code-block:: sh

	[global]    # 主配置段
		workgroup = WORKGROUP          # 设置主机工作组
		log level = 1                  # 设置日志文件安全级别为1
		log file = /var/log/samba/%m   # 设置日志文件名称，%m以IP地址为名称
		max log size = 50              # 设置日志文件最大容量50KB，0表示不限制
		security = user                # 以用户名和密码验证方式访问
		passdb backend = smbpasswd     # 定义用户后台类型
		load printers = no             # 关闭打印共享功能
	[homes]    # 设置共享资源的显示目录为homes
		comment = Home Directories     # 设置共享描述信息
		path = /home/%u                # 设置共享资源的源目录，%u表示登陆的samba用户名
		browseable = yes               # 显示目录可见，即访问samba资源根目录时homes目录可见
		read only = No                 # 设置非只读权限
		create mask = 0775             # 访问共享资源创建的文件权限为0775(默认为0744)，属主为访问用户
		directory mode = 0775          # 访问共享资源创建的目录权限为0775(默认为0755)，属主为访问用户
		writable = yes                 # 设置该共享资源有可写权限
	[happy]       # 设置共享资源的显示目录为happy
		comment = dataFile             # 设置共享描述信息
		path = /home/happy             # 设置共享资源的源目录
		browseable = yes               # 显示目录可见，即访问samba资源根目录时happy目录可见
		read only = no                 # 设置非只读权限
		create mask = 0775             # 访问共享资源创建的文件权限为0775(默认为0744)，属主为访问用户
		directory mode =0775           # 访问共享资源创建的目录权限为0775(默认为0755)，属主为访问用户
		writable = yes                 # 设置该共享资源有可写权限
	[zhangwuyi]   # 设置共享资源的显示目录为zhangwuyi
		comment = dataFile             # 设置共享描述信息
		path = /home/zhangwuyi     	   # 设置共享资源的源目录
		browseable = yes           	   # 显示目录可见，即访问samba资源根目录时zhangwuyi目录可见
		read only = no             	   # 设置非只读权限
		create mask = 0775         	   # 访问共享资源创建的文件权限为0775(默认为0744)，属主为访问用户
		directory mode =0775       	   # 访问共享资源创建的目录权限为0775(默认为0755)，属主为访问用户
		writable = yes             	   # 设置该共享资源有可写权限

针对以上配置的访问逻辑如下所示

- 存在\ ``[homes]``\ 字段

	- \ ``\\192.168.80.134``\ ：使用该种方式访问时，会直接进入samba共享资源的根目录下，该目录下会显示名为\ ``homes``\ 、\ ``happy``\ 、\ ``zhangwuyi``\ 的目录，以及以访问用户为目录名的目录；此时可以随意对每个目录进行读写操作；每个目录下存放的内容如下

		- \ ``homes``\ ：存放\ ``path``\ 参数值\ ``/home/%u``\ 下的内容，%u为访问的samba用户
		- \ ``happy``\ ：存放\ ``path``\ 参数值\ ``/home/happy``\ 下的内容
		- \ ``zhangwuyi``\ ：存放\ ``path``\ 参数值\ ``/home/zhangwuyi``\ 下的内容
		- 以访问用户为目录名的目录存放的是\ ``/home/%u``\ 下的内容，%u为访问的samba用户
	- \ ``\\192.168.80.134\ShareName``\ ：使用该种方式访问时，进入目录名为\ ``ShareName``\ 的目录；只不过该目录下存放的内容不一样；将\ ``ShareName``\ 与配置文件中的共享资源名(\ ``homes/happy/zhangwuyi``\ )进行比较，如果有相同字段名，则内容为该字段下\ ``path``\ 参数值指定的目录内容；如果\ ``ShareName``\ 没有匹配到相应字段，会默认匹配到\ ``homes``\ 字段，内容为该字段下\ ``path``\ 参数值指定的目录内容；此时也可以回退到samba共享资源的根目录下，随意对每个目录进行读写操作

		- \ ``ShareName``\ 为\ ``happy``\ 时会显示进入\ ``happy``\ 目录下，该目录的内容为\ ``/home/happy``\ 目录下的内容
		- \ ``ShareName``\ 为\ ``zhangwuyi``\ 时会显示进入\ ``zhangwuyi``\ 目录下，该目录的内容为\ ``/home/zhangwuyi``\ 目录下的内容
		- \ ``ShareName``\ 为\ ``test``\ 时会显示进入\ ``test``\ 目录下，该目录的内容为\ ``/home/%u``\ 目录下的内容，%u为访问的samba用户
- 不存在\ ``[homes]``\ 字段

	- \ ``\\192.168.80.134``\ ：使用该种方式访问时，会直接进入samba共享资源的根目录下，该目录下会显示名为\ ``happy``\ 、\ ``zhangwuyi``\ 的目录；此时可以随意对每个目录进行读写操作；每个目录下存放的内容如下

		- \ ``happy``\ ：存放\ ``path``\ 参数值\ ``/home/happy``\ 下的内容
		- \ ``zhangwuyi``\ ：存放\ ``path``\ 参数值\ ``/home/zhangwuyi``\ 下的内容
	- \ ``\\192.168.80.134\ShareName``\ ：使用该种方式访问时，进入目录名为\ ``ShareName``\ 的目录；只不过该目录下存放的内容不一样；将\ ``ShareName``\ 与配置文件中的共享资源名(\ ``homes/happy/zhangwuyi``\ )进行比较，如果有相同字段名，则内容为该字段下\ ``path``\ 参数值指定的目录内容；如果\ ``ShareName``\ 没有匹配到相应字段，会报错无法访问资源；此时也可以回退到samba共享资源的根目录下，随意对每个目录进行读写操作

		- \ ``ShareName``\ 为\ ``happy``\ 时会显示进入\ ``happy``\ 目录下，该目录的内容为\ ``/home/happy``\ 目录下的内容
		- \ ``ShareName``\ 为\ ``zhangwuyi``\ 时会显示进入\ ``zhangwuyi``\ 目录下，该目录的内容为\ ``/home/zhangwuyi``\ 目录下的内容
		- \ ``ShareName``\ 为\ ``test``\ 时，没有匹配到相应的共享字段，会报错无法访问资源

上述配置方式保证了共享资源只能被合法samba用户访问，即只要是合法samba用户就能访问所有的共享资源目录；如果想控制每个共享资源目录只能被指定samba用户访问，每个共享目录下需设定\ ``valid users``\ 参数

- \ ``[happy]``\ 字段下添加\ ``valid users = happy``\ ，表示只有名为\ ``happy``\ 的samba用户才能访问该目录
- \ ``[zhangwuyi]``\ 字段下添加\ ``valid users = zhangwuyi``\ ，表示只有名为\ ``zhangwuyi``\ 的samba用户才能访问该目录

修改完配置文件后可以通过以下命令来检测配置文件是否有语法错误，如果配置文件存在语法错误，是无法启动samba服务的

- \ ``testparm -v``\ 

如下所示表示配置文件存在语法错误(samba 4.2以后版本不支持share参数值设定)

.. figure:: ../images/10.png

如下所示表示配置文件没有语法错误

.. figure:: ../images/11.png

要想对共享目录进行读写操作，需要将共享目录权限设置为最大值

- \ ``chmod 777 /home/zhangwuyi``\ 
- \ ``chmod 777 /home/happy``\ 


.. _sambau:

0x03 添加samba用户
~~~~~~~~~~~~~~~~~~~~~~

添加samba用户的方式有两种

- \ ``smbpasswd``\ 命令：该命令会给系统用户设置SMB密码建立samba用户；不过使用该命令建立samba用户必须先建立系统用户，否则samba找不到系统用户则创建失败；该命令主要维护\ ``/var/lib/samba/private/smbpasswd``\ 密码文件

	- \ ``adduser happy``\ ：创建系统用户
	- \ ``passwd happy``\ ：为系统用户设定密码；该密码可不用设置
	- \ ``smbpasswd –a happy``\ ：将刚创建的系统用户设置为samba用户并设置密码
	- \ ``smbpasswd -d happy``\ ：冻结刚创建的samba用户，该用户不能在登录了
	- \ ``smbpasswd -e happy``\ ：恢复冻结的samba用户，让冻结的用户可以在使用
	- \ ``smbpasswd -n happy``\ ：将samba用户的密码设置成空
	- \ ``smbpasswd -x happy``\ ：删除samba用户
- \ ``pdbedit``\ 命令：该命令直接建立SMB独立用户；该命令建立samba用户不需要先建立系统用户；该命令主要维护\ ``/var/lib/samba/private/{passdb.tdb,secrets.tdb}``\ 数据库文件

	- \ ``pdbedit –a happy``\ ：新建Samba账户
	- \ ``pdbedit –x happy``\ ：删除Samba账户
	- \ ``pdbedit –L``\ ：列出Samba用户列表，读取\ ``passdb.tdb``\ 数据库文件
	- \ ``pdbedit –Lv``\ ：列出Samba用户列表的详细信息
	- \ ``pdbedit –c "[D]" –u happy``\ ：暂停该Samba用户的账号
	- \ ``pdbedit –c "[]" –u happy``\ ：恢复该Samba用户的账号

此处我们使用\ ``smbpasswd``\ 命令添加samba用户(因为我们设置的\ ``passdb backend``\ 参数值为\ ``smbpasswd``\ ；如果该参数值为\ ``tdbsam``\ ，则使用\ ``pdbedit``\ 命令创建samba用户)

- \ ``adduser happy``\ ：创建系统用户happy
- \ ``passwd happy``\ ：为系统用户happy设置系统密码
- \ ``adduser zhangwuyi``\ ：创建系统用户zhangwuyi
- \ ``passwd zhangwuyi``\ ：为系统用户zhangwuyi设置系统密码
- \ ``smbpasswd -a happy``\ ：将系统用户happy添加成samba用户并设置相应密码
- \ ``smbpasswd -a zhangwuyi``\ ：将系统用户zhangwuyi添加成samba用户并设置相应密码
- \ ``cat /var/lib/samba/private/smbpasswd``\ ：查看新添加的samba用户和密码是否已经同步到相应的密码文件中

.. figure:: ../images/12.png

.. figure:: ../images/13.png

.. figure:: ../images/14.png

.. _firel:

0x04 配置防火墙
~~~~~~~~~~~~~~~~~~~~~~~~~

Samab服务开启之前需要关闭两个服务

- iptables防火墙(如果你熟悉可以不关闭，放行smb的端口即可，SAMBA服务TCP端口139,445  UDP端口137,138)

	- centos6版本防火墙默认使用\ ``iptables``\ 

		- \ ``service iptables stop``\ ：临时关闭iptables防火墙
		- \ ``chkconfig iptables off``\ ：禁止开机启动iptables防火墙
	- centos7版本防火墙默认使用\ ``firewalld``\ 

		- \ ``systemctl stop firewalld``\ ：临时关闭firewall防火墙
		- \ ``systemctl status firewalld``\ ：查看firewall防火墙状态
		- \ ``systemctl disable firewalld``\ ：禁止开机启动firewall防火墙 
- selinux服务

	- 方式一：打开samba保护开关，该配置永久生效

		- \ ``getsebool -a | grep samba_export``\ ：查看selinux对samba共享的保护开关
		- \ ``setsebool -P samba_export_all_rw=on``\ ：用setsebool打开SELinux开关
		- \ ``setsebool -P samba_export_all_ro=on``\ ：用setsebool打开SELinux开关
	- 方式二：永久关闭selinux服务，该配置永久生效

		- \ ``vim /etc/selinux/config``\ ：将\ ``SELINUX``\ 的值设置为\ ``disabled``\  



.. _servl:

0x05 启动samba服务
~~~~~~~~~~~~~~~~~~~~~~~~~

启动samba服务的命令有

- centos7版本

	- \ ``service smb status``\ 或\ ``systemctl status smb.service``\ ：查看samba服务的状态
	- \ ``service smb start``\ 或\ ``systemctl start smb.service``\ ：启动samba服务
	- \ ``service smb stop``\ 或\ ``systemctl stop smb.service``\ ：停止samba服务
	- \ ``service smb restart``\ 或\ ``systemctl restart smb.service``\ ：重启samba服务
	- \ ``systemctl enable smb.service``\ ：设置开机启动samba服务


.. _clils:

0x06 客户端访问
~~~~~~~~~~~~~~~~~~~~~~~~~

window下访问

- 直接访问(首次访问需要输入samba用户以及密码进行验证，同时该验证信息将会存储到系统列表中，以后通过该方式访问都会从系统列表中读取该缓存信息进行验证)

.. figure:: ../images/16.png

.. figure:: ../images/17.png

- 磁盘映射(如果勾选了\ ``使用其它凭据连接``\ ，则会在连接时要求输入新的samba用户以及密码进行验证；如果没有勾选，和上述直接访问方式一样将会从系统缓存列表中读取已经验证过的信息进行验证)

.. figure:: ../images/18.png

.. figure:: ../images/19.png

linux下访问

- 在线安装\ ``samba-client``\ 客户端软件，使用\ ``smbclient``\ 命令访问samba服务器

	- RedHat/CentOS发行版：\ ``yum -y install samba-client``\ 
- 查看samba服务器共享目录

	- \ ``smbclient -L //192.168.80.134 -U happy``\ ：使用happy用户来查看Samba服务器有那些共享
	- \ ``smbclient //192.168.80.134/happy -U happy``\ ：使用happy用户来访问happy共享文件夹
- 将samba服务器共享目录挂载到本地目录下进行访问操作

	- \ ``mount -t cifs -o username=happy //192.168.80.134/happy /mnt/share``\ 


.. _faqll:

0x07 常见FAQ
~~~~~~~~~~~~~~

- FAQ1：不允许一个用户使用一个以上用户名与一个服务器或共享资源的多重连接(windows下)

	- 原因：局域网中共享文件夹时，如果我们使用过某个用户名和密码与服务器的共享文件夹进行过连接，并且选择记住了密码。当我们需要更改其他账号进入或映射一个未授权的共享文件夹时，就会出现以上错误提示
	- 解决方法：在cmd下运行\ ``net use * /del /y``\ 来清除系统记录列表(\ ``net user 用户名 密码 /add``\ 命令可以用来创建用户)

.. figure:: ../images/15.png