项目工程管理
=================

- \ `创建c项目 <#creatpro>`_\ 

	- \ `svn仓库检出创建 <#svncreate>`_\ 
	- \ `eclipse手动新建 <#eclcreate>`_\ 
- \ `设置项目选项 <#prooption>`_\ 
- \ `代码编辑 <#codeedit>`_ \
- \ `代码编译 <#codecompile>`_\ 
- \ `代码提交 <#codesubmit>`_\ 


.. _creatpro:

0x00 创建c项目
~~~~~~~~~~~~~~~~

创建c项目的方法有两种：

- eclipse导入svn资源库全部源码，从svn资源库检出指定目录创建项目(\ **创建速度较慢，不推荐**\ )
- svn下载全部源码到服务器，然后eclipse指定路径手动创建项目(\ **创建速度较快，推荐此种方法**\ )

在创建项目之前，我们需要弄清楚两个概念：\ ``项目(Project)``\ 和\ ``工作空间(Workspace)``\ 

- \ ``Workspace``\ 是\ ``Project``\ 的集合
- \ ``Project``\ 是源代码文件的集合

\ ``Eclipse``\ 是通过项目和工作区间的概念来组织源代码的

.. _howwork:

\ ``Workspace``\ 具有以下特点

- 该目录下\ ``.metadata``\ 目录存储了该工作空间中所有项目和插件的配置信息(包括语法高亮颜色，字体等)，此目录的存在标识该目录是有效的工作区间
- \ ``.metadata``\ 目录中还包含了以\ ``.log``\ 命名的文件。此文件将包含在运行\ ``Eclipse``\ 时可能抛出的所有错误或异常
- 设置工作空间的方法就是：在首次启动\ ``Eclipse``\ 时，根据提示设置工作空间的默认位置，此时可以不用勾选\ ``Use this as the default and do not ask again``\ ，这样每次启动时都会弹窗显示，根据需要选择；如果首次勾选了该项同时需要修改工作空间，选择\ ``Window->Preferences->General->Startup and Shutdow->Workspace``\ ，勾选\ ``Prompt for workspaces on startup``\ ，最后重启，会和首次启动\ ``Eclipse``\ 一样，弹窗提示设置默认工作区间
- 切换工作空间的方法是：选择\ ``File->Switch Workspace->Other``\ ，可以随时切换到其他工作空间或创建新的工作空间
- 将当前工作空间的所有设置复制到新工作空间中的简单方法

	- 选择\ ``File->Export``\ 
	- 在打开的对话框中，选择\ ``General->Preferences``\ ，然后单击\ ``Next``
	- 选择\ ``Export All``\ ，提供文件的导出路径，然后单击\ ``Finish``\ 。工作空间的所有设置都将被保存到指定路径中
	- 切换到新工作空间，选择\ ``File->Import``\ ，然后选择\ ``General->Preferences``\ ，指向刚刚保存的设置文件并单击\ ``Finish``\ 。您的设置将被导入到新工作空间中

\ ``Project``\ 有以下特点

- 项目必须放在工作空间目录下才可以被Eclipse使用
- 项目目录中存放的是源码文件，即独立的应用程序或模块

.. _svncreate:

0x0000 svn仓库检出创建
~~~~~~~~~~~~~~~~~~~~~~~~~~

在eclipse启动时，指定默认工作空间路径(设置工作空间路径参考:\ `workspace <#howwork>`_\ )

- 工作空间设置为\ `映射区间 <../3-conf/index.html#workl>`_\ ：适用于\ `检出c项目 <#proname>`_\ 时，勾选\ ``作为工作空间中的项目检出``\ 选项，此时项目源码路径就是映射区间路径下，这样可以实现本机和服务器之间的代码同步，然后在服务器上进行编译
- 工作空间设置为\ `映射区间 <../3-conf/index.html#workl>`_\ 和本地路径都行：适用于\ `检出c项目 <#proname>`_\ 时，勾选\ ``作为新项目检出，并使用新建项目向导进行配置``\ 选项，此时项目源码路径可以重新指定为\ `映射区间 <../3-conf/index.html#workl>`_\ ，也可以实现本机和服务器之间的代码同步，然后在服务器上进行编译

这里我们将默认工作区间设置为\ `映射区间 <../3-conf/index.html#workl>`_\ 

.. figure:: ../images/41.png

找到\ ``package->semp_system->app``\ 目录，右键，选择检出为

.. figure:: ../images/42.png

.. _proname:

修改项目名称

.. figure:: ../images/43.png

检出完成后，点击右上角\ ``Open Perspective``\ 按钮，打开\ ``c/c++(default)``\ 或直接点击右上角的\ ``c/c++(default)``\ 按钮，就可以看到新建的工程

.. figure:: ../images/44.png

.. figure:: ../images/45.png

由于检出的文件太多，可能导致\ ``eclipse``\ 变慢，因此可以删除部分用不到的文件

进入工程目录\ ``Z:\sempOS\test.sempos``\ ，删除下列文件

- \ ``src/drv/tools``\ 
- \ ``src/drv/ufal``\ 
- \ ``src/drv/bdd/dc6408``\  
- \ ``src/mng/doc``\ 

右键选中项目，选择\ ``New->Convert to a C/C++ Project``\ 

.. figure:: ../images/49.png

选择转换为\ ``C项目``\ ，项目类型为\ ``Makefile``\ ，工具链选\ ``Cross GCC``\ 

.. figure:: ../images/50.png

转换完成后，项目图标会变为C

.. figure:: ../images/51.png


.. _eclcreate:

0x0001 eclipse手动新建
~~~~~~~~~~~~~~~~~~~~~~~

在服务器上创建一个目录，用来单独存放所有项目的源码；然后使用svn命令下载源码到该目录下，重命名项目名称

- \ ``mkdir sempOS && cd sempOS``\ 
- \ ``svn co http://192.168.1.6/svn/sempos/trunk``\ 
- \ ``mv -r trunk/ DC6408_RPC``\ 

在eclipse启动时，指定默认工作空间路径为\ `映射区间 <../3-conf/index.html#workl>`_\ 和本地路径都行；因为手动创建c项目时，项目源码路径可以重新指定为\ `映射区间 <../3-conf/index.html#workl>`_\ ，实现本机和服务器之间的代码同步，然后在服务器上进行编译

这里我们将默认工作区间设置为本地路径(设置工作空间路径参考:\ `workspace <#howwork>`_\ )

.. figure:: ../images/62.png

选择\ ``File->New->Makefile Project with Existing Code``\ 

.. figure:: ../images/63.png

指定项目名称，已存在项目源码路径，选择\ ``C``\ 语言和\ ``Cross Gcc``\ 工具链

.. figure:: ../images/64.png

创建项目成功后如下所示，项目图标会变为C

.. figure:: ../images/65.png


.. _prooption:

0x01 设置项目选项
~~~~~~~~~~~~~~~~~~

如果在项目中需要实现函数、变量等的跳转，项目的\ ``include头文件路径``\ 中需要包含以下路径下的头文件

- \ ``/usr/include``\ 
- \ ``/usr/include/x86_64-linux-gnu``\ 
- \ ``/usr/local/include``\ 
- \ ``/usr/lib/gcc/x86_64-linux-gnu/4.8/include``\ 
- \ ``trunk/package/semp_system/common/include``\ 

对于前4类头文件，需要将这些目录下的头文件拷贝到映射区间，然后添加到eclipse中

- \ ``mkdir -p ~/sempOS/standard_inc/usr/{local,lib/gcc/x86_64-linux-gnu/4.8}``\ 
- \ ``cp -r /usr/include ~/sempOS/standard_inc/usr``\ 
- \ ``cp -r /usr/local/include ~/sempOS/standard_inc/usr/local``\ 
- \ ``cp -r /usr/lib/gcc/x86_64-linux-gnu/4.8/include ~/sempOS/standard_inc/usr/lib/gcc/x86_64-linux-gnu/4.8``\ 

右键选中项目，选择\ ``Properties``\ ，修改\ ``C/C++ General``\ 下的\ ``Paths and Symbols``\ 设置，将上述头文件的路径添加到include头文件包含路径中

.. figure:: ../images/60.png

对于最后一类头文件

- 使用svn仓库检出方法创建项目时，需要将\ ``common``\ 目录检出为一个新的工程然后添加

.. figure:: ../images/55.png

.. figure:: ../images/56.png

.. figure:: ../images/58.png

- 使用eclipse手动新建方法创建项目时，只需要添加下载源码\ ``common``\ 目录下的头文件即可

.. figure:: ../images/66.png


添加完成后如下：

- 使用svn仓库检出方法创建项目效果图

.. figure:: ../images/61.png

- 使用eclipse手动新建方法创建项目效果图

.. figure:: ../images/67.png

\ **注意**\ ：在有些源码文件还需要包含其它的头文件才可以，例如：\ ``cli_demo.c``\ 源文件，需要包含\ ``/usr/include/x86_64-linux-gnu/asm/``\ 头文件路径，因为该目录下的\ ``signal.h``\ 头文件声明了\ ``SIGKILL``\ 、\ ``SIGPIPE``\ 等宏，如果不包含就会报错显示。此时需要灵活处理，可以使用以下命令查看报错字段声明的头文件，然后包含该头文件路径即可

- \ ``grep -rn "SIGKILL" ~/sempOS/standard_inc/usr``\  

根据需求，可以在\ ``Eclipse``\ 中添加符号定义，效果相当于Makefile中定义的符号；右键选中项目，选择\ ``Properties``\ ，修改\ ``C/C++ General``\ 下的\ ``Paths and Symbols``\ 设置；点击\ ``Symbols``\ 项，添加自定义符号

.. figure:: ../images/52.png

.. figure:: ../images/53.png

为避免不需要分析的代码，加快速度，可以添加代码路径过滤条件；在\ ``Paths and Symbols``\ 设置；点击\ ``Source Location``\ 项

.. figure:: ../images/54.png

最后右键选中项目，选择\ ``Index->Rebuild``\ 重新建立索引，至此工程选项全部设置完成

.. figure:: ../images/59.png


.. _codeedit:

0x02 代码编辑
~~~~~~~~~~~~~~~~

默认快捷键

- 回退：\ ``alt+left``\ 
- 前进：\ ``alt+right``\ 
- 代码注释：\ ``ctrl+/``\ 
- 自动补全：\ ``alt+/``\ 
- 代码格式化：\ ``ctrl+shift+F``\ 
- 跳转到调用的地方

	- \ ``ctrl+shift+G``\ 
	- \ ``鼠标右键+Reference+Project``\ 
- 跳转到定义或声明的地方

	- \ ``ctrl+鼠标左键``\ 
	- \ ``F3``\ 
- tooltip显示定义：\ ``F2``\ 
- 打开搜索对话框：\ ``ctrl+h``\ 
- 全局查找并替换：\ ``ctrl+r``\ 
- 全局字体放大：\ ``ctrl+=``\ 
- 全局字体缩小：\ ``ctrl+-``\ 
- 折叠所有代码：\ ``ctrl+shift+小键盘/``\ 
- 展开所有代码：\ ``ctrl+shift+小键盘*``\ 
- 撤销操作：\ ``ctrl+z``\ 

快捷键自定义方法：\ ``Window->Preferences->General->Keys``\ ，在搜索框搜索\ ``Command``\ 快捷键名称或\ ``Binding``\ 当前绑定快捷键名，搜索后点击该条记录，编辑下方的\ ``Binding``\ 项，点击应用关闭即可

修改跳转到调用的地方：\ ``ctrl+c``\ 

.. figure:: ../images/68.png

修改跳转到定义或声明的地方：\ ``ctrl+d``\ 

.. figure:: ../images/69.png

修改全局查找并替换：\ ``ctrl+r``\ 

.. figure:: ../images/70.png

修改打开搜索对话框：\ ``ctrl+f``\ 

.. figure:: ../images/71.png

修改代码自动补全：\ ``tab``\ 

.. figure:: ../images/72.png


.. _codecompile:

0x03 代码编译
~~~~~~~~~~~~~~

代码编译是在服务器上进行编译，此处之后再补充

.. _codesubmit:

0x04 代码提交
~~~~~~~~~~~~~~~

代码提交时使用svn版本控制工具进行提交，可参考\ `svn版本控制 <../../../2-vcs/index.html>`_\ 

