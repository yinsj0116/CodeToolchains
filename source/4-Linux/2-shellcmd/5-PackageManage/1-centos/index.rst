CentOS软件包管理
=================

CentOS系统中常用的软件包管理方式有以下几种方式

.. toctree::
   :titlesonly:
   :glob:


   1-rpm/index
   2-yum/index
   3-srccode/index
